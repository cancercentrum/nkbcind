% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/nkbc-omv-kontaktssk-02.R
\docType{data}
\name{nkbc02}
\alias{nkbc02}
\title{Objekt som definierar "Kontaktsjuksköterska"}
\format{
An object of class \code{keynkbcind} (inherits from \code{nkbcind}) of length 14.
}
\usage{
nkbc02
}
\description{
\code{nkbc02} är ett objekt av klassen \code{keynkbcind} (ärver från \code{nkbcind})
som definierar nyckelindikatorn
"Patienten har erbjudits, i journalen dokumenterad, kontaktsjuksköterska".
}
\examples{
stop_if_not_valid_object(nkbc02)
print(nkbc02)
}
\keyword{datasets}
