# nkbcind

Verktyg för beräkning av kvalitetsindikatorer och
populationskarakteristik för Nationellt kvalitetsregister från
bröstcancer (NKBC).

Detta R-paket är en **central** plats för definition, implementering och
dokumentation av **beräkning av kvalitetsindikatorer och
populationskarakteristik** från NKBC i **alla** utdata-kanaler.

-   Användning på INCA-plattformen tillsammans med R-paketet
    [nkbcgeneral](https://bitbucket.org/cancercentrum/nkbcgeneral)
    -   Onlinerapporter innanför inloggning på INCA-plattformen med
        R-paketet
        [rccShiny](https://bitbucket.org/cancercentrum/rccshiny)
    -   NKBC Koll på läget (KPL), med R-paketet
        [rccKPL](https://bitbucket.org/cancercentrum/rcckpl)
    -   NKBC Vården i siffror, med R-paketet
        [incavis](https://bitbucket.org/cancercentrum/incavis)
-   Användning lokalt på RCC Stockholm-Gotland tillsammans med R-paketet
    [nkbcgeneral](https://bitbucket.org/cancercentrum/nkbcgeneral)
    -   Framtagande av NKBC Interaktiva Årsrapport med R-paketet
        [rccShiny](https://bitbucket.org/cancercentrum/rccshiny)

Jfr
<https://www.cancercentrum.se/samverkan/vara-uppdrag/statistik/kvalitetsregisterstatistik/>

## Installation

``` r
if (!requireNamespace("remotes")) {
  install.packages("remotes")
}

remotes::install_bitbucket("cancercentrum/nkbcind")
```

## Användning

``` r
library(nkbcind)
```

Läs in ögonblicksbild av NKBC exporterad från INCA.

``` r
load(
  file.path(Sys.getenv("BRCA_DATA_DIR"), "2022-09-02", "nkbc_nat_avid 2022-09-02 07-08-17.RData")
)
```

Generell förbearbetning av NKBC-data.

``` r
df_nkbc_nat_avid_w_d_vars <- df |>
  dplyr::mutate(dplyr::across(tidyselect::where(is.factor), as.character)) |>
  dplyr::rename_with(
    # Döp om INCA:s variabelsuffix "_Värde" till "_Varde" eftersom
    # R-paket inte kan byggas om variabelnamn innehåller specialtecken
    stringr::str_replace, tidyselect::ends_with("_Värde"),
    pattern = "_Värde", replacement = "_Varde"
  ) |>
  nkbcgeneral::clean_nkbc_data() |>
  nkbcgeneral::mutate_nkbc_derived_vars()
```

Skapa generell dataram för redovisning av kvalitetsindikatorer och
populationskarakteristik.

``` r
df_nkbc_nat_avid_w_nkbcind_d_vars <- df_nkbc_nat_avid_w_d_vars |>
  nkbcind::mutate_nkbcind_derived_vars() |>
  dplyr::mutate(period = lubridate::year(a_diag_dat)) |>
  dplyr::filter(period %in% 2008:2021)
```

### Exempel: Screeningupptäckt bröstcancer

Titta på objektet `nkbc01` som definierar kvalitetsindikatorn
"Screeningupptäckt bröstcancer" och används i de olika utdata-kanalerna
från NKBC.

``` r
print(nkbc01)
#> $id
#> [1] "nkbc01"
#> 
#> $kortnamn
#> [1] "nkbc_diag_screening_01"
#> 
#> $lab
#>                                 sv                                 en 
#>    "Screeningupptäckt bröstcancer" "Screening-detected breast cancer" 
#> 
#> $pop
#>                                               sv 
#> "kvinnliga fall i åldrarna 40–74 år vid diagnos" 
#>                                               en 
#>      "femal cases aged 40–74 years at diagnosis" 
#> 
#> $filter_pop
#> function(x, ...) {
#>     dplyr::filter(
#>       x,
#>       # Enbart fall i åldrarna 40–74 år vid diagnos
#>       a_pat_alder >= 40,
#>       a_pat_alder <= 74,
#> 
#>       # Enbart kvinnliga fall
#>       KON_VALUE == 2
#>     )
#>   }
#> <bytecode: 0x0000022260e674a0>
#> <environment: namespace:nkbcind>
#> 
#> $mutate_outcome
#> function(x, ...) {
#>     dplyr::mutate(x,
#>       outcome = dplyr::case_when(
#>         a_diag_screening_Varde == 0L ~ FALSE,
#>         a_diag_screening_Varde == 1L ~ TRUE,
#>         a_diag_screening_Varde == 98L ~ NA
#>       )
#>     )
#>   }
#> <bytecode: 0x0000022260e6f198>
#> <environment: namespace:nkbcind>
#> 
#> $target_values
#> [1] 60 70
#> 
#> $period_dat_var
#> [1] "a_diag_dat"
#> 
#> $sjhkod_var
#> [1] "a_inr_sjhkod"
#> 
#> $other_vars
#> [1] "a_pat_alder" "d_invasiv"  
#> 
#> $om_indikatorn
#> $om_indikatorn$sv
#> [1] "Mammografiscreening erbjuds alla kvinnor mellan 40–74 år."
#> 
#> $om_indikatorn$en
#> [1] "Mammography screening is offered to all women aged 40-74."
#> 
#> 
#> $vid_tolkning
#> $vid_tolkning$sv
#> [1] "Definitionen av \"screeningupptäckt fall\" kan enligt erfarenhet tolkas olika vilket kan påverka siffrorna. Här avses enbart de fall som diagnostiserats i samband med en kallelse till den regionsorganiserade screeningmammografin."
#> 
#> $vid_tolkning$en
#> NULL
#> 
#> 
#> $teknisk_beskrivning
#> NULL
#> 
#> attr(,"class")
#> [1] "keynkbcind" "nkbcind"
```

Extrahera information om kvalitetsindikatorn såsom den presenteras i
rccShiny-baserade interaktiva rapporter.

``` r
cat(
  paste(
    nkbcind::get_rccShiny2_arg_outcomeTitle(nkbc01)["sv"],
    nkbcind::get_rccShiny2_arg_textBeforeSubtitle(nkbc01)["sv"],
    sep = "\n"
  )
)
#> Screeningupptäckt bröstcancer
#> Bland kvinnliga fall i åldrarna 40–74 år vid diagnos.
```

Om indikatorn.

``` r
cat(
  nkbcind::get_rccShiny2_arg_description(nkbc01, show_target_values = TRUE)[["sv"]][1]
)
#> Mammografiscreening erbjuds alla kvinnor mellan 40–74 år.
#> </p><p>
#> Målnivåer: 60% (låg) 70% (hög)
#> </p><p>
#> För motsvarande indikator med flera urvalsmöjligheter, se under ’Diagnostik’.
```

Att tänka på vid tolkning.

``` r
cat(
  nkbcind::get_rccShiny2_arg_description(nkbc01, show_target_values = TRUE)[["sv"]][2]
)
#> Definitionen av "screeningupptäckt fall" kan enligt erfarenhet tolkas olika vilket kan påverka siffrorna. Här avses enbart de fall som diagnostiserats i samband med en kallelse till den regionsorganiserade screeningmammografin.
#> </p><p>
#> Ett fall per bröst kan rapporterats till Nationellt kvalitetsregister för bröstcancer (NKBC). Det innebär att samma person kan finnas med i statistiken två gånger.
#> </p><p>
#> Skövde och Lidköpings sjukhus presenteras tillsammans som Skaraborg.
#> </p><p>
#> Malmö och Lunds sjukhus presenteras tillsammans som Lund/Malmö.
```

Teknisk beskrivning.

``` r
cat(
  nkbcind::get_rccShiny2_arg_description(nkbc01, show_target_values = TRUE)[["sv"]][3]
)
#> Population: kvinnliga fall i åldrarna 40–74 år vid diagnos.
#> </p><p>
#> Uppgifterna redovisas uppdelat på anmälande sjukhus.
```

Specifik databearbetning för kvalitetsindikatorn.

``` r
df_nkbc01 <- df_nkbc_nat_avid_w_nkbcind_d_vars |>
  nkbcind::filter_pop(nkbc01)() |>
  nkbcind::mutate_outcome(nkbc01)()
```

Titta på bearbeatad data.

``` r
df_nkbc01 |>
  dplyr::mutate(dplyr::across(dplyr::any_of("KON_VALUE"), as.factor)) |>
  dplyr::mutate(dplyr::across(tidyselect::ends_with("_Varde"), as.factor)) |>
  dplyr::select(KON_VALUE, a_pat_alder, a_diag_screening_Varde, outcome) |>
  summary()
#>  KON_VALUE  a_pat_alder    a_diag_screening_Varde  outcome       
#>  2:90758   Min.   :40.00   0   :33929             Mode :logical  
#>            1st Qu.:52.00   1   :56497             FALSE:33929    
#>            Median :61.00   98  :  233             TRUE :56497    
#>            Mean   :59.97   NA's:   99             NA's :332      
#>            3rd Qu.:68.00                                         
#>            Max.   :74.00
```

``` r
df_nkbc01 |>
  dplyr::select(period, outcome) |>
  table(useNA = "ifany")
#>       outcome
#> period FALSE TRUE <NA>
#>   2008  2552 3123   37
#>   2009  2428 3243   39
#>   2010  2389 3831   45
#>   2011  2528 3915   32
#>   2012  2336 4091   27
#>   2013  2351 4265   36
#>   2014  2399 4355   44
#>   2015  2381 4206   19
#>   2016  2392 4098   16
#>   2017  2367 4394   10
#>   2018  2357 4259   10
#>   2019  2481 4459    6
#>   2020  2431 3794    9
#>   2021  2537 4464    2
```

Jfr <https://statistik.incanet.se/brostcancer/> \> Diagnostik \>
Screeningupptäckt bröstcancer
