#' För argumentet periodDefaultStart till rccShiny::rccShiny2
#'
#' @aliases get_rccShiny2_arg_periodDefaultStart get_rccShiny2_arg_periodDefaultStart.nkbcind
#' @export
get_rccShiny2_arg_periodDefaultStart <- function(x, ...) {
  UseMethod("get_rccShiny2_arg_periodDefaultStart")
}

#' @rdname get_rccShiny2_arg_periodDefaultStart
#' @inheritParams get_rccShiny2_arg_description
#' @inheritParams filter_incl_period
#' @return Ett objekt som kan användas för argumentet `periodDefaultStart` till [`rccShiny::rccShiny2()`].
#' @details
#' ```{r child = "man/rmd/nkbc-rccShiny-default-selection.Rmd"}
#' ```
#' @seealso [get_rccShiny2_arg_periodDefaultEnd()], [get_rccShiny2_arg_periodSplitDefault()]
#' @examples
#' # "Screeningupptäckt bröstcancer" (nkbc01)
#'
#' # Presentation utanför INCA
#' get_rccShiny2_arg_periodDefaultStart(nkbc01)
#' get_rccShiny2_arg_periodDefaultEnd(nkbc01)
#' get_rccShiny2_arg_periodSplitDefault(nkbc01)
#'
#' # Presentation på INCA
#' get_rccShiny2_arg_periodDefaultStart(nkbc01, inca = TRUE)
#' get_rccShiny2_arg_periodDefaultEnd(nkbc01, inca = TRUE)
#' get_rccShiny2_arg_periodSplitDefault(nkbc01, inca = TRUE)
#' @export
get_rccShiny2_arg_periodDefaultStart.nkbcind <- function(x,
                                                         ...,
                                                         report_end_year = lubridate::year(lubridate::today()) - 1,
                                                         inca = FALSE) {
  stopifnot(
    is.null(report_end_year) | is.numeric(report_end_year),
    is.null(report_end_year) | length(report_end_year) == 1,
    is.null(report_end_year) | report_end_year %in% 2008:lubridate::year(lubridate::today())
  )
  stopifnot(is.logical(inca), !is.na(inca))

  periodDefaultEnd <- get_rccShiny2_arg_periodDefaultEnd(x, report_end_year = report_end_year, inca = inca)

  if (inca) {
    if (x$id %in% c("nkbc30", "nkbc30b")) {
      # Överlevnadsrapporter
      periodDefaultStart <- 2008
    } else {
      # Övriga rapporter
      periodDefaultStart <- periodDefaultEnd - 4
    }
  } else {
    if (x$id %in% c("nkbc30", "nkbc30b")) {
      # Överlevnadsrapporter
      periodDefaultStart <- 2008
    } else {
      # Övriga rapporter
      periodDefaultStart <- periodDefaultEnd
    }
  }

  return(periodDefaultStart)
}
