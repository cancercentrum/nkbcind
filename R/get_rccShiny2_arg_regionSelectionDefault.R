#' För argumentet regionSelectionDefault till rccShiny::rccShiny2
#'
#' @aliases get_rccShiny2_arg_regionSelectionDefault get_rccShiny2_arg_regionSelectionDefault.nkbcind
#' @export
get_rccShiny2_arg_regionSelectionDefault <- function(x, ...) {
  UseMethod("get_rccShiny2_arg_regionSelectionDefault")
}

#' @rdname get_rccShiny2_arg_regionSelectionDefault
#' @inheritParams get_rccShiny2_arg_description
#' @param login_region `NULL` eller ett numeriskt värde med regiontillhörighet för inloggad användare, ett heltal mellan 1 och 6. Standardvärde är `NULL`.
#' @return Ett objekt som kan användas för argumentet `regionSelectionDefault` till [`rccShiny::rccShiny2()`].
#' @details
#' ```{r child = "man/rmd/nkbc-rccShiny-default-selection.Rmd"}
#' ```
#' @seealso [get_rccShiny2_arg_regionSelection()], [get_rccShiny2_arg_geoUnitsDefault()], [get_rccShiny2_arg_geoUnitsHospitalSelected()], [geo_units_vars()]
#' @export
get_rccShiny2_arg_regionSelectionDefault.nkbcind <- function(x,
                                                             ...,
                                                             login_region = NULL,
                                                             inca = FALSE) {
  stopifnot(
    is.null(login_region) | is.numeric(login_region),
    is.null(login_region) | length(login_region) == 1,
    is.null(login_region) | login_region %in% 1:6
  )
  stopifnot(is.logical(inca), !is.na(inca))

  if (inca) {
    if (all(geo_units_vars(x) %in% "region")) {
      # Situation med enbart redovisningsnivån sjukvårdsregion
      regionSelectionDefault <- NULL
    } else {
      regionSelectionDefault <- login_region
    }
  } else {
    regionSelectionDefault <- NULL
  }

  return(regionSelectionDefault)
}
