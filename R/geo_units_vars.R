#' Extrahera geo_units_vars
#'
#' @aliases geo_units_vars geo_units_vars.nkbcind
#' @export
geo_units_vars <- function(x, ...) {
  UseMethod("geo_units_vars")
}

#' @rdname geo_units_vars
#' @inheritParams stop_if_not_valid_object
#' @return En teckenvektor (_character vector_) med `geo_units_vars`. Om `x$geo_unit_vars` är `NULL` returneras standardvärdet `c("region", "landsting", "sjukhus")`.
#' @examples
#' # "Screeningupptäckt bröstcancer" (nkbc01)
#' geo_units_vars(nkbc01)
#'
#' # Jfr med
#' nkbc01$geo_units_vars
#'
#' # "Observerad 5-årsöverlevnad (nkbc30)
#' geo_units_vars(nkbc30)
#'
#' # Jfr med
#' nkbc30$geo_units_vars
#' @export
geo_units_vars.nkbcind <- function(x, ...) {
  if (!is.null(x$geo_units_vars)) {
    x$geo_units_vars
  } else {
    c("region", "landsting", "sjukhus")
  }
}
