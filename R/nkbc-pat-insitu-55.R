#' Objekt som definierar "Enbart cancer in situ"
#'
#' `nkbc55` är ett objekt av klassen `nkbcind` som definierar kvalitetsindikatorn
#' "Enbart cancer in situ".
#'
#' @examples
#' stop_if_not_valid_object(nkbc55)
#' print(nkbc55)
#' @export
nkbc55 <- list(
  id = "nkbc55",
  kortnamn = "nkbc_pat_insitu_55",
  lab = c(
    sv = "Enbart cancer in situ"
  ),
  pop =
    c(
      sv = "alla anmälda fall"
    ),
  filter_pop = function(x, ...) {
    x
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(x,
      outcome = dplyr::case_when(
        d_invasiv_Varde == 1L ~ FALSE,
        d_invasiv_Varde == 2L ~ TRUE
      )
    )
  },
  period_dat_var = "a_diag_dat",
  sjhkod_var = "d_pat_sjhkod",
  other_vars = c("a_pat_alder", "d_screening"),
  om_indikatorn = NULL,
  vid_tolkning = NULL,
  teknisk_beskrivning = NULL
)
class(nkbc55) <- "nkbcind"
