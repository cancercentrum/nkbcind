#' Objekt som definierar "Ki67 <= 20"
#'
#' `nkbc64` är ett objekt av klassen `nkbcind` som definierar kvalitetsindikatorn
#' "Ki67 <= 20".
#'
#' @examples
#' stop_if_not_valid_object(nkbc64)
#' print(nkbc64)
#' @export
nkbc64 <- list(
  id = "nkbc64",
  kortnamn = "nkbc_pat_ki67le20_64",
  lab =
    c(
      sv = "Ki67 <= 20 %"
    ),
  pop =
    c(
      sv = "alla anmälda invasiva fall"
    ),
  filter_pop = function(x, ...) {
    dplyr::filter(
      x,
      # Ki67 tillkom som nationell variabel 2013
      lubridate::year(a_diag_dat) >= 2013,

      # Enbart fall med invasiv cancer
      d_invasiv_Varde == 1
    )
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(
      x,
      outcome = d_pad_ki67proc <= 20
    )
  },
  period_dat_var = "a_diag_dat",
  sjhkod_var = "d_pat_sjhkod",
  other_vars = c("a_pat_alder", "d_screening", "d_prim_beh"),
  om_indikatorn = NULL,
  vid_tolkning = NULL,
  teknisk_beskrivning = NULL
)
class(nkbc64) <- "nkbcind"
