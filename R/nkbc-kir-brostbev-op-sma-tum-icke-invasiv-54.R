#' Objekt som definierar "Bröstbevarande op vid små icke-invasiva tumörer"
#'
#' `nkbc54` är ett objekt av klassen `nkbcind` som definierar kvalitetsindikatorn
#' "Bröstbevarande operation vid små icke-invasiva tumörer".
#'
#' @examples
#' stop_if_not_valid_object(nkbc54)
#' print(nkbc54)
#' @export
nkbc54 <- list(
  id = "nkbc54",
  kortnamn = "nkbc_kir_brostbev_op_sma_tum_icke-invasiva_54",
  lab = c(
    sv = "Bröstbevarande operation vid små icke-invasiva tumörer",
    en = "Breast-conserving surgery for small non-invasive tumours"
  ),
  pop = c(
    sv = "primärt opererade fall med icke-invasiv tumör <=20 mm",
    en = "primarily operated cases with non-invasive tumor <=20 mm"
  ),
  filter_pop = function(x, ...) {
    dplyr::filter(
      x,
      # Variabeln extent infördes mitten av 2014
      lubridate::year(a_diag_dat) >= 2015,

      # Enbart primärt opererade fall
      !is.na(op_kir_dat),
      d_prim_beh_Varde == 1,

      # Enbart fall med bröstkirurgi
      op_kir_brost_Varde %in% c(1, 2, 4),

      # Enbart fall med enbart cancer in situ
      d_invasiv_Varde == 2,

      # Enbart DCIS
      op_pad_insitusnomed_Varde %in% c(10, 40, 60, 67),

      # Enbart fall med extent <= 20mm
      d_max_extent <= 20,

      # Ej fall med fjärrmetastaser vid diagnos
      !(a_tnm_mklass_Varde %in% 10)
    )
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(x,
      outcome = dplyr::case_when(
        op_kir_brost_Varde == 1L ~ TRUE,
        op_kir_brost_Varde %in% c(2L, 4L) ~ FALSE
      )
    )
  },
  target_values = c(90, 95),
  period_dat_var = "op_kir_dat",
  sjhkod_var = "op_inr_sjhkod",
  other_vars = "a_pat_alder",
  om_indikatorn = list(
    sv = paste(
      "Ett bröstbevarande ingrepp och strålbehandling är standardingrepp för majoriteten av tidigt upptäckta bröstcancrar.",
      "Tumörens egenskaper, form och storlek på bröstet spelar roll vid val av operationsmetod."
    ),
    en = paste(
      "Breast-conserving surgery and radiation therapy are standard procedures for the majority of early-detected breast cancers.",
      "The size and location of the tumour as well as the size of the breast affects the choice of type of surgery."
    )
  ),
  vid_tolkning = NULL,
  teknisk_beskrivning = NULL
)
class(nkbc54) <- c("keynkbcind","nkbcind")
