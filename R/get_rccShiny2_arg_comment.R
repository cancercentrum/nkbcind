#' För argumentet comment till rccShiny::rccShiny2
#'
#' @aliases get_rccShiny2_arg_comment get_rccShiny2_arg_comment.nkbcind
#' @export
get_rccShiny2_arg_comment <- function(x, ...) {
  UseMethod("get_rccShiny2_arg_comment")
}

#' @rdname get_rccShiny2_arg_comment
#' @inheritParams get_rccShiny2_arg_description
#' @return Ett objekt som kan användas för argumentet `comment` till [`rccShiny::rccShiny2()`].
#' @examples
#' # Kommentar för "Observerad 5-årsöverlevnad" (nkbc30)
#' get_rccShiny2_arg_comment(nkbc30)
#'
#' # Kommentar för "Observerad 5-årsöverlevnad" (nkbc30), två språk
#' get_rccShiny2_arg_comment(nkbc30, language = c("sv", "en"))
#' @export
get_rccShiny2_arg_comment.nkbcind <- function(x,
                                              ...,
                                              language = "sv",
                                              inca = FALSE) {
  stopifnot(is.character(language), all(language %in% c("sv", "en")))
  stopifnot(is.logical(inca), !is.na(inca))

  if (!is.null(x$comment)) {
    comment <- x$comment[language]
  } else {
    # Använd standardvärde för argumentet comment till rccShiny2
    comment <- c(sv = "", en = "")[language]
  }

  return(comment)
}
