#' För argumentet targetValues till rccShiny::rccShiny2
#'
#' @aliases get_rccShiny2_arg_targetValues get_rccShiny2_arg_targetValues.nkbcind
#' @export
get_rccShiny2_arg_targetValues <- function(x, ...) {
  UseMethod("get_rccShiny2_arg_targetValues")
}

#' @rdname get_rccShiny2_arg_targetValues
#' @inheritParams get_rccShiny2_arg_description
#' @return Ett objekt som kan användas för argumentet `targetValues` till [`rccShiny::rccShiny2()`].
#' @examples
#' # "Screeningupptäckt bröstcancer" (nkbc01)
#' get_rccShiny2_arg_targetValues(nkbc01)
#' @export
get_rccShiny2_arg_targetValues.nkbcind <- function(x,
                                                   ...,
                                                   show_target_values = FALSE,
                                                   inca = TRUE) {
  stopifnot(is.logical(show_target_values), !is.na(show_target_values))
  stopifnot(is.logical(inca), !is.na(inca))

  if (show_target_values) {
    # Situation med redovisning av indikator *med* målvärden,
    targetValues <- x$target_values
  } else {
    targetValues <- NULL
  }

  return(targetValues)
}
