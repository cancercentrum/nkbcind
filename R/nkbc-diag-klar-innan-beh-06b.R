#' Objekt som definierar "Fastställd diagnos innan behandling"
#'
#' `nkbc06b` är ett objekt av klassen `keynkbcind` (ärver från `nkbcind`)
#' som definierar nyckelindikatorn "Fastställd diagnos innan behandling".
#'
#' @examples
#' stop_if_not_valid_object(nkbc06b)
#' print(nkbc06b)
#' @export
nkbc06b <- list(
  id = "nkbc06b",
  kortnamn = "nkbc_diag_klar_innan_beh_06b",
  lab = c(
    sv = "Fastställd diagnos innan start av behandling",
    en = "Definitive diagnosis before start of treatment"
  ),
  pop = c(
    sv = "behandlande (opererade eller onkologisk behandlade) fall utan fjärrmetastaser vid diagnos",
    en = "treated (operated och oncologically treated) cases without distant metastasis at diagnosis"
  ),
  filter_pop = function(x, ...) {
    dplyr::filter(
      x,
      # Inrapporteringen av given onkologisk behandling har påbörjats vid olika tidpunkter i olika sjukvårdsregioner.
      # Fr.o.m. 2012 anses rapportering ha skett nationellt
      lubridate::year(a_diag_dat) >= 2012,

      # Enbart behandlade fall
      d_prim_beh_Varde %in% c(1,2),

      # Ej fall med fjärrmetastaser vid diagnos
      !(a_tnm_mklass_Varde %in% 10)
    )
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(x,
      outcome = dplyr::case_when(
        a_diag_preopmorf_Varde == 0L ~ FALSE,
        a_diag_preopmorf_Varde == 1L ~ TRUE,
        a_diag_preopmorf_Varde == 98L ~ NA
      )
    )
  },
  target_values = c(95, 98),
  period_dat_var = "a_diag_dat",
  sjhkod_var = "d_prim_beh_sjhkod",
  other_vars = c("a_pat_alder", "d_invasiv", "d_prim_beh"),
  om_indikatorn = list(
    sv = "Fastställd diagnos innan behandlingsstart är viktig för rätt behandling i rätt tid och i rätt turordning (primär operation eller preoperativ behandling) och för att kirurgin ska kunna utföras i en seans.",
    en = "A definitive diagnosis before starting treatment is important for planning and implementing treatment in the correct order (primarily operated or preoperative oncological treatment) and avoiding reoperations."
  ),
  vid_tolkning = list(
    sv = paste(
      "För att undvika alltför långa utredningstider kan det ibland vara nödvändigt att påbörja behandlingen av patienten innan diagnosen är helt fastställd.",
      "Fastställd diagnos måste vägas mot tidsåtgång."
    ),
    en = paste(
      "To reduce patients’ waiting time, it may sometimes be necessary to start the treatment  of the patioent before the diagnosis is fully established.",
      "A definitive diagnosis must be weighed against time consumption "
    )
  ),
  teknisk_beskrivning = NULL
)
class(nkbc06b) <- c("keynkbcind", "nkbcind")
