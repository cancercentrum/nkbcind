#' Bearbeta RCC S-G:s dataram med sjukhusdata
#'
#' @param x RCC S-G:s dataram med sjukhusdata.
#' @param ... Ytterligare argument som skickas till eller från andra metoder (används inte för närvarande).
#' @return Bearbetad dataram med sjukhusdata.
#' @seealso [add_sjhdata()]
#' @examples
#' \dontrun{
#' # Läs in RCC S-G:s dataram med sjukhusdata
#' load(
#'   # Lokalt på RCC S-G
#'   "G:/Hsf/RCC-Statistiker/_Generellt/INCA/Data/sjukhusKlinikKoder/sjukhuskoder.RData"
#'   # På INCA
#'   # file.path(Sys.getenv("ScriptPath"), "Stockholm", "Gemensamt", "Data", "sjukhuskoder.RData")
#' )
#'
#' df_sjhdata <- mutate_sjhdata(sjukhuskoder)
#' df_sjhdata
#' }
#' @export
mutate_sjhdata <- function(x, ...) {
  stopifnot(is.data.frame(x))

  df_sjhdata <- x |>
    dplyr::rename(
      "df_rcc1_sjhdata_var_sjukhuskod" = "sjukhuskod",
      "df_rcc1_sjhdata_var_region" = "region",
      "df_rcc1_sjhdata_var_sjukhusnamnOrginal" = "sjukhusnamnOrginal",
      "df_rcc1_sjhdata_var_sjukhusnamn" = "sjukhusnamn",
      "df_rcc1_sjhdata_var_uppdaterad" = "uppdaterad"
    ) |>
    dplyr::mutate(
      sjukhuskod = as.integer(df_rcc1_sjhdata_var_sjukhuskod),
      sjukhus = dplyr::case_when(
        df_rcc1_sjhdata_var_sjukhusnamn %in% c("Enhet utan INCA-rapp", "VC/Tjänsteläkare") ~ NA_character_,
        TRUE ~ df_rcc1_sjhdata_var_sjukhusnamn
      ),
      sjukhuskod_inledande_2_siffror = as.character(df_rcc1_sjhdata_var_sjukhuskod) |>
        stringr::str_sub(1, 2) |>
        as.integer(),
      landsting = dplyr::case_when(
        sjukhuskod_inledande_2_siffror %in% c(
          seq(10, 13),
          seq(21, 28),
          30,
          seq(41, 42),
          seq(50, 57),
          seq(61, 65)
        ) ~ sjukhuskod_inledande_2_siffror,
        # Fulfix Stockholms bröstklinik och Bröstmot. Christinakliniken Sh
        sjukhuskod %in% c(97333, 97563) ~ 10L,
        # Annars
        TRUE ~ NA_integer_
      ),
      region = dplyr::case_when(
        df_rcc1_sjhdata_var_region == "Sthlm/Gotland" ~ 1L,
        df_rcc1_sjhdata_var_region == "Uppsala/Örebro" ~ 2L,
        df_rcc1_sjhdata_var_region == "Sydöstra" ~ 3L,
        df_rcc1_sjhdata_var_region == "Syd" ~ 4L,
        df_rcc1_sjhdata_var_region == "Väst" ~ 5L,
        df_rcc1_sjhdata_var_region == "Norr" ~ 6L,
        TRUE ~ NA_integer_
      )
    ) |>
    dplyr::select(
      sjukhuskod,
      sjukhus,
      landsting,
      region
    )

  return(df_sjhdata)
}
