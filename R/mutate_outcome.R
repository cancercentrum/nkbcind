#' Extrahera funktion för att beräkna utfallsvariabel
#'
#' @aliases mutate_outcome mutate_outcome.nkbcind
#' @export
mutate_outcome <- function(x, ...) {
  UseMethod("mutate_outcome")
}

#' @rdname mutate_outcome
#' @inheritParams get_rccShiny2_arg_outcomeTitle
#' @return En funktion som beräknar utfallsvariabeln för kvalitetsindikatorn.
#' @examples
#' # "Typ av axillkirurgi" (nkbc45)
#' mutate_outcome(nkbc45)
#' @export
mutate_outcome.nkbcind <- function(x, ...) {
  x$mutate_outcome
}

#' @rdname mutate_outcome
#' @inheritParams get_rccShiny2_arg_outcomeTitle
#'
#' # Nyckelindikatorn "Välgrundad misstanke om cancer till primär operation" (nkbc15)
#' mutate_outcome(nkbc15)
#' mutate_outcome(nkbc15, show_target_values = TRUE)
#' @export
mutate_outcome.keynkbcind <- function(x,
                                      ...,
                                      show_target_values = FALSE) {
  stopifnot(is.logical(show_target_values), !is.na(show_target_values))

  if (show_target_values) {
    if (!is.null(x$prop_within_value) & length(x$prop_within_value) == 1 & outcome_var(x) == "outcome") {
      # x antas vara en ledtid
      # TODO Hantera situation när outcome_var inte heter "outcome"?
      function(y, ...) {
        y |>
          x$mutate_outcome(...) |>
          dplyr::mutate(
            outcome = (outcome <= x$prop_within_value)
          )
      }
    } else {
      x$mutate_outcome
    }
  } else {
    x$mutate_outcome
  }
}
