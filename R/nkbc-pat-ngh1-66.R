#' Objekt som definierar "NHG 1"
#'
#' `nkbc66` är ett objekt av klassen `nkbcind` som definierar kvalitetsindikatorn
#' "NHG 1".
#'
#' @examples
#' stop_if_not_valid_object(nkbc66)
#' print(nkbc66)
#' @export
nkbc66 <- list(
  id = "nkbc66",
  kortnamn = "nkbc_pat_ngh1_66",
  lab =
    c(
      sv = "NHG 1"
    ),
  pop =
    c(
      sv = "alla anmälda invasiva fall"
    ),
  filter_pop = function(x, ...) {
    dplyr::filter(
      x,
      # Enbart fall med invasiv cancer
      d_invasiv_Varde == 1
    )
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(
      x,
      outcome = dplyr::case_when(
        d_pad_nhg_Varde == 1L ~ TRUE,
        d_pad_nhg_Varde %in% c(2L, 3L) ~ FALSE,
        d_pad_nhg_Varde %in% c(97L, 98L, NA_integer_) ~ NA
      )
    )
  },
  period_dat_var = "a_diag_dat",
  sjhkod_var = "d_pat_sjhkod",
  other_vars = c("a_pat_alder", "d_screening", "d_prim_beh"),
  om_indikatorn = NULL,
  vid_tolkning = list(
    sv = "Variabeln för histologisk grad från mellan-/grovnålsbiopsi togs bort 2017 och återinfördes 2020 i registret."
  ),
  teknisk_beskrivning = NULL
)
class(nkbc66) <- "nkbcind"
