#' Beskrivning för rccKPL
#'
#' @aliases get_rccKPL_description get_rccKPL_description.nkbcind
#' @export
get_rccKPL_description <- function(x, ...) {
  UseMethod("get_rccKPL_description")
}

#' @rdname get_rccKPL_description
#' @inheritParams get_rccShiny2_arg_description
#' @return En textsträng som beskriver kvalitetsindikatorn.
#' @seealso [get_rccKPL_name()] [rccKPL::KPL()]
#' @examples
#' # "Screeningupptäckt bröstcancer" (nkbc01)
#' get_rccKPL_description(nkbc01)
#'
#' # "Välgrundad misstanke om cancer till primär operation" (nkbc15)
#' get_rccKPL_description(nkbc15)
#' @export
get_rccKPL_description.nkbcind <- function(x, ...) {
  # Adopted from https://stackoverflow.com/a/56125845
  str_to_lower2 <- function(x) {
    str_keep_upper <- c(
      "ER",
      "ER-positivitet",
      "PR",
      "PR-positivitet",
      "HER2",
      "HER2-positivitet",
      "IHC",
      "ISH",
      "(ISH)",
      "Ki67",
      "NHG"
    )
    paste(lapply(strsplit(x, " "), function(y) ifelse(y %in% str_keep_upper, y, tolower(y)))[[1]], collapse = " ")
  }

  paste(
    c(
      paste0("Andel med ", str_to_lower2(get_rccKPL_name(x)), " bland ", x$pop["sv"], ".") |>
        # stringr::str_to_sentence(locale = "sv") |>
        stringr::str_replace_all("min vårdplan", "Min Vårdplan"),
      if (!is.null(x$anvander_missca) && x$anvander_missca == TRUE) {
        "Datum för välgrundad misstanke om cancer tillkom som variabel 2016 och innan detta har datum för 1:a kontakt använts."
      },
      paste0(
        "Fall beskrivs utifrån ",
        dplyr::case_when(
          period_dat_var(x) %in% "a_diag_dat" ~ "diagnosdatum",
          period_dat_var(x) %in% "d_pre_onk_dat" ~ "startdatum för preoperativ onkologisk behandling",
          period_dat_var(x) %in% "op_kir_dat" ~ "operationsdatum"
        ),
        " och ",
        dplyr::case_when(
          sjhkod_var(x) %in% "a_inr_sjhkod" ~
            "anmälande sjukhus",
          sjhkod_var(x) %in% "op_inr_sjhkod" ~
            "opererande sjukhus",
          sjhkod_var(x) %in% "d_opans_sjhkod" ~
            "opererande sjukhus, och om detta saknas, anmälande sjukhus",
          sjhkod_var(x) %in% "d_pat_sjhkod" ~
            "opererande sjukhus för primärt opererade fall, annars anmälande sjukhus",
          sjhkod_var(x) %in% "d_prim_beh_sjhkod" ~
            "sjukhus ansvarig för primär behandling",
          sjhkod_var(x) %in% c("post_inr_sjhkod", "pre_inr_sjhkod", "d_onk_sjhkod") ~
            "sjukhus där onkologisk behandling ges",
          sjhkod_var(x) %in% c("d_onkpreans_sjhkod", "d_onkpostans_sjhkod") ~
            "rapporterande sjukhus där onkologisk behandling ges, och om detta saknas, sjukhus ansvarigt för rapportering av onkologisk behandling, sjukhus för onkologisk behandling, anmälande sjukhus",
          sjhkod_var(x) %in% "d_uppfans_sjhkod" ~
            "sjukhus ansvarigt för rapportering av uppföljning, och om detta saknas, sjukhus för onkologisk behandling, sjukhus ansvarigt för rapportering av onkologisk behandling, opererande sjukhus, anmälande sjukhus"
        ),
        "."
      ),
      if (!is.null(x$target_values)) {
        dplyr::case_when(
          length(x$target_values) == 1 ~
            paste0("Målnivå: ", x$target_values[1], "%."),
          length(x$target_values) == 2 ~
            paste0("Målnivåer: ", x$target_values[1], "% (låg) ", x$target_values[2], "% (hög).")
        )
      }
    ),
    collapse = " "
  )
}
