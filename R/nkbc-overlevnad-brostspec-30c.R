#' Objekt som definierar "Bröstcancerspecifik överlevnad"
#'
#' `nkbc30c` är ett objekt av klassen `nkbcind` som definierar kvalitetsindikatorn
#' "Bröstcancerspecifik överlevnad".
#'
#' @examples
#' stop_if_not_valid_object(nkbc30c)
#' print(nkbc30c)
#' @export
nkbc30c <- list(
  id = "nkbc_30c",
  kortnamn = "nkbc_overlevnad_brostspec_30c",
  lab = c(
    sv = "Bröstcancerspecifik överlevnad",
    en = "Breast cancer specific survival"
    ),
  pop = c(
    sv = "alla anmälda fall",
    en = "all reported cases"
    ),
  filter_pop = function(x, ...) {
    dplyr::filter(
      x,
      lubridate::year(a_diag_dat) <= 2022,
      TRUE
    )
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(
      x,
      survdthrfdt = lubridate::ymd(a_diag_dat),
      survdthrf = "diagnos",
      survdthtm =
        dplyr::if_else(
          lubridate::ymd(VITALSTATUSDATUM_ESTIMAT) > survdthrfdt,
          lubridate::interval(survdthrfdt, lubridate::ymd(VITALSTATUSDATUM_ESTIMAT)) / lubridate::years(1),
          0
        ),
      survdthtmu = "år",
      dthstat =
        as.factor(ifelse(lubridate::ymd(VITALSTATUSDATUM_ESTIMAT) > survdthrfdt & as.integer(dod_i_bc_dors %in% 1), 1,
                         ifelse(dod_i_bc_dors %in% 0 & VITALSTATUS %in% 1, 2, 0L))),
      dikotom_dummy1 = TRUE
    )
  },
  outcome = c("dikotom_dummy1"),
  outcome_title = list(
    sv = c("Bröstcancerspecifik överlevnad"),
    en = c("Breast cancer specific survival")
  ),
  sjhkod_var = "a_inr_sjhkod",
  other_vars = c("a_pat_alder", "d_invasiv", "d_trigrp"),
  other_vars_inca = c("a_pat_alder", "d_invasiv", "d_trigrp", "d_tstad", "d_nstad", "d_mstad", "d_tnm_stadium_subgrp"),
  comment =
    c(
      sv = "Observera att analysen inte är justerad för skillnader i case-mix, socioekonomi etc.",
      en = "Note that the analysis is not adjusted for differences in case mix, socioeconomics, etc."
    ),
  om_indikatorn = list(
    sv = paste(
      "Överlevnadskurva som visar andelen dödsfall för de två utfallen brösctancerspecifik död respektive död av annan underliggande orsak.",
      "Dödsorsaker hämtades under 2023 och därmed är kalenderår 2022 det senast heltäckande året.",
      "Observera att analysen inte är justerad för skillnader i case-mix, socioekonomi etc."
    ),
    en = paste(
      "Survival curve presenting the proportion of deaths for the two outcomes breast cancer-specific death and death from another underlying cause.",
      "Death causes was retrieved during 2023 and thereby the calendar year 2022 is the lastest year with full coverage.",
      "Note that the analysis is not adjusted for differences in case mix, socioeconomics, etc."
    )
  ),
  vid_tolkning = list(
    sv = paste("Med antal i riskzon menas antalet fall som vid given tidpunkt lever och har haft diagnosen i minst det antal år som tidpunkten visar.",
               "Observerade överlevnaden är sannolikheten att leva efter en viss tidpunkt, oavsett utfall (syns endast i tabellen).",
               "I början rapporterades T0N0M0 in till registret, instruktionerna ändrades 2013."),
    en = paste("Number at risk shows the number of cases that at a given time are alive and have had the diagnosis for at least the number of years at that time.",
               "Observed survival is the probability of being alive after a certain time, regardless of outcome (only visible in the table).",
               "At the start T0N0M0 was reported to the register, the instructions changed in 2013.")
  ),
  teknisk_beskrivning = list(
    sv = paste("Dödsorsaker är hämtade från Dödsorsaksregistret(Socialstyrelsen).",
               "Cumulative incident function med Aalen-Johansen estimatorn användes vid beräkningen av tabell och graf"),
    en = paste("Cause of death is retrived from National Cause of Death Register(Socialstyrelsen).",
               "Cumulative incident function with Aalen-Johansen estimate was used when calculating the tables and graph"
               )
  )
)
class(nkbc30c) <- "nkbcind"