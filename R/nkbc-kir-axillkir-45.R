#' Objekt som definierar "Axillkirurgi"
#'
#' `nkbc45` är ett objekt av klassen `nkbcind` som definierar kvalitetsindikatorn
#' "Typ av axillkirurgi".
#'
#' @examples
#' stop_if_not_valid_object(nkbc45)
#' print(nkbc45)
#' @export
nkbc45 <- list(
  id = "nkbc45",
  kortnamn = "nkbc_kir_axillkir_45",
  lab = c(
    sv = "Typ av axillkirurgi",
    en = "Type of axillary surgery"
  ),
  lab_short = c(
    sv = "Axillkirurgi",
    en = "Axillary surgery"
  ),
  pop = c(
    sv = "opererade fall med axillingrepp och utan fjärrmetastaser vid diagnos",
    en = "operated cases with axillary surgery without distant metastasis at diagnosis"
  ),
  filter_pop = function(x, ...) {
    dplyr::filter(
      x,
      # Enbart opererade fall
      !is.na(op_kir_dat),

      # Enbart fall med axillingrepp
      op_kir_axill_Varde == 1,

      # Ej fall med fjärrmetastaser vid diagnos
      !(a_tnm_mklass_Varde %in% 10),
      
      # Ej operation eller fjärrmetastas/-er vid diagnos när op_kir=NA
      !(a_planbeh_typ_Varde %in% c(3, 7, 8) & is.na(op_kir_Varde))
    )
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(x,
      outcome = factor(
        dplyr::case_when(
          op_kir_axilltyp_Varde == 1L ~ 1L,
          op_kir_axilltyp_Varde == 2L ~ 2L,
          op_kir_axilltyp_Varde == 3L ~ 3L,
          op_kir_axilltyp_Varde %in% c(98L, 4L, NA_integer_) ~ NA_integer_
        ),
        levels = c(1L, 3L, 2L),
        labels = c("Enbart SN", "SN och utrymning", "Enbart utrymning")
      ),
      outcome_en = forcats::fct_recode(
        outcome,
        "SN only" = "Enbart SN",
        "SN and axillary clearance" = "SN och utrymning",
        "Axillary clearance only" = "Enbart utrymning"
      )
    )
  },
  sjhkod_var = "op_inr_sjhkod",
  other_vars = c("a_pat_alder", "d_invasiv", "d_nstad", "d_prim_beh"),
  other_vars_inca = c("a_pat_alder", "d_invasiv", "d_nstad", "d_prim_beh"),
  om_indikatorn = list(
    sv = c(
      "Axillkirurgi utförs för att klarlägga om spridning finns till armhålans lymfkörtlar.",
      "Förenklat utförs sentinel node biopsi vid kliniskt nodnegativ sjukdom, axillutrymning vid kliniskt nodpositiv sjukdom och SN och utrymning vid kliniskt nodnegativ sjukdom där sentinel node inte hittas eller innehåller metastas."
    ),
    en = c(
      "Axillary surgery is performed to investigate if there is tumour spread to the lymph nodes of the armpit.",
      "In short, sentinel node biopsy is performed in clinically node-negative disease, axillary clearance in clinically node-positive disease and SN followed by axillary clearance in clinical node-negative disease where the sentinel node contains metastasis or is not found."
    )
  ),
  vid_tolkning = NULL,
  teknisk_beskrivning = NULL
)
class(nkbc45) <- "nkbcind"
