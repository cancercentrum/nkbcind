#' Beskrivning för rccKPL (utfasad version)
#'
#' @aliases kpl_description kpl_description.nkbcind
#' @keywords internal
#' @export
kpl_description <- function(x, ...) {
  UseMethod("kpl_description")
}

#' @rdname kpl_description
#' @inheritParams get_rccKPL_description
#' @details
#' `kpl_description()` fasades ut i nkbcgind 0.20.0
#' Använd `get_rccKPL_description()` istället.
#' @seealso [get_rccKPL_description()]
#' @keywords internal
#' @export
kpl_description.nkbcind <- function(x, ...) {
  # lifecycle::deprecate_stop("0.20.0", "kpl_description()", "get_rccKPL_description()")
  stop(
    paste(
      c(
        "`kpl_description()` was deprecated in nkbcind 0.20.0 and is now defunct.",
        "Please use `get_rccKPL_description()` instead."
      ),
      collapse = "\n"
    )
  )
}
