#' Objekt som definierar "Screeningupptäckt bröstcancer"
#'
#' `nkbc06` är ett objekt av klassen `keynkbcind` (ärver från `nkbcind`)
#' som definierar nyckelindikatorn "Screeningupptäckt bröstcancer".
#'
#' @examples
#' stop_if_not_valid_object(nkbc01)
#' print(nkbc01)
#' @export
nkbc01 <- list(
  id = "nkbc01",
  kortnamn = "nkbc_diag_screening_01",
  lab = c(
    sv = "Screeningupptäckt bröstcancer",
    en = "Screening-detected breast cancer"
  ),
  pop = c(
    sv = "kvinnliga fall i åldrarna 40–74 år vid diagnos",
    en = "femal cases aged 40–74 years at diagnosis"
  ),
  filter_pop = function(x, ...) {
    dplyr::filter(
      x,
      # Enbart fall i åldrarna 40–74 år vid diagnos
      a_pat_alder >= 40,
      a_pat_alder <= 74,

      # Enbart kvinnliga fall
      KON_VALUE == 2
    )
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(x,
      outcome = dplyr::case_when(
        a_diag_screening_Varde == 0L ~ FALSE,
        a_diag_screening_Varde == 1L ~ TRUE,
        a_diag_screening_Varde == 98L ~ NA
      )
    )
  },
  target_values = c(60, 70),
  period_dat_var = "a_diag_dat",
  sjhkod_var = "a_inr_sjhkod",
  other_vars = c("a_pat_alder", "d_invasiv"),
  om_indikatorn = list(
    sv = "Mammografiscreening erbjuds alla kvinnor mellan 40–74 år. Mammografiscreening ger möjlighet till diagnos i tidigare skede av sjukdomen och förbättrar överlevnaden. 
    Som screeningupptäckta avses de fall som diagnostiserats i samband med den regionsorganiserade screeningmammografin.",
    en = "Mammography screening is offered to all women aged 40-74. Mammography screening provides the opportunity for diagnosis at an earlier stage of the disease and improves survival. 
    The cases diagnosed in connection with the regionally organized screening mammography are referred to as screening detected."
  ),
  vid_tolkning = NULL,
  teknisk_beskrivning = NULL
)
class(nkbc01) <- c("keynkbcind", "nkbcind")
