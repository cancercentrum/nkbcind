#' Objekt som definierar "Abemaciclib vid högrisk"
#'
#' 	Abemaciclib vid högrisk ER-pos, HER2-neg BC (>=5 cm+pN1 eller pN2 eller grad III+pN1)
#'
#' `nkbc82` är ett objekt av klassen `nkbcind` som definierar kvalitetsindikatorn
#' "Anti-HER2-riktad behandling"
#'
#' @examples
#' stop_if_not_valid_object(nkbc50)
#' print(nkbc82)
#' @export
nkbc82 <- list(
  id = "nkbc82",
  kortnamn = "nkbc_onk_bisfos_postmeno_82",
  lab = c(
    sv = "Bisfosfonater",
    en = "Bisphosphonates"
  ),
  pop = c(
    sv = "postmenopausala, nodpositiva fall utan fjärrmetastaser vid diagnos",
    en = "postmenopausal, node-positive cases without distant metastases at diagnosis"
  ),
  pop_short = c(
    sv = "postmenopausala, nodpositiva fall utan fjärrmetastaser vid diagnos",
    en = "postmenopausal, node-positive cases without distant metastases at diagnosis"
  ),
  filter_pop = function(x, ...) {
    dplyr::filter(
      x,
      # Inrapporteringen av given onkologisk behandling har påbörjats vid olika tidpunkter i olika sjukvårdsregioner.
      # Fr.o.m. 2012 anses rapportering ha skett nationellt
      lubridate::year(a_diag_dat) >= 2012,

      # Postmenopaustal
      a_pat_mensstat_Varde == 2,
      
      # Enbart fall med spridning till lymfkörtlar,
      # preoperativt verifierad lymfkörtelmetastas och/eller
      # lymfkörtlar med metastas (ej fall med enbart mikrometastas) från något axillingrepp (oavsett primär behandling)
      (
        a_pad_snmet_Varde %in% 1 |
          (
            op_pad_lglmetant > 0 &
              !((op_pad_snmakrometant == 0 & op_pad_snmikrometant > 0 & op_pad_lglmetant == op_pad_snmikrometant) %in% TRUE)
          )
      ),

      # Ej fall med fjärrmetastaser vid diagnos
      !(a_tnm_mklass_Varde %in% 10)
    )
  },
  mutate_outcome = function(x, ...) {
    dplyr::mutate(x,
                  # Går på det som finns, pre eller postop. Om det ena saknas antas samma som finns för det andra.
                  outcome = dplyr::case_when(
                    post_bis_Varde == 1 ~ TRUE,
                    post_bis_Varde == 0 ~ FALSE,
                    .default = NA
                  )

    )
  },
  period_dat_var = "a_diag_dat",
  sjhkod_var = "d_onk_sjhkod",
  other_vars = c("a_pat_alder", "d_er"),
  other_vars_inca = c("a_pat_alder", "d_er"),
  om_indikatorn = list(
    sv = paste(
      "Postoperativ bisfosfonatbehandling sänker risken för skelettmetastasering och rekommenderas till postmenopausala patienter med primär lymfkörtelmetastaserande sjukdom oavsett hormonreceptorstatus."
    ),
    en = paste(
      "Postoperative bisphosphonate treatment lowers the risk of bone metastasis and is recommended for postmenopausal patients with primary lymph node metastatic disease regardless of hormone receptor status."
      )
  ),
  vid_tolkning = NULL,
  efterslap_pga_inrapp_onk_beh = TRUE,
  teknisk_beskrivning = NULL
)
class(nkbc82) <- "nkbcind"