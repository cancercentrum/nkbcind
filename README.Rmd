---
output:
  md_document:
    variant: markdown
---

```{r, echo = FALSE}
knitr::opts_chunk$set(
  warning = FALSE,
  message = FALSE,
  collapse = TRUE,
  comment = "#>"
  # fig.path = "man/figures/README-"
)
```

# nkbcind

Verktyg för beräkning av kvalitetsindikatorer och populationskarakteristik för Nationellt kvalitetsregister från bröstcancer (NKBC).

Detta R-paket är en **central** plats för definition, implementering och dokumentation av **beräkning av kvalitetsindikatorer och populationskarakteristik** från NKBC i **alla** utdata-kanaler.

- Användning på INCA-plattformen tillsammans med R-paketet [nkbcgeneral](https://bitbucket.org/cancercentrum/nkbcgeneral)
  - Onlinerapporter innanför inloggning på INCA-plattformen med R-paketet [rccShiny](https://bitbucket.org/cancercentrum/rccshiny)
  - NKBC Koll på läget (KPL), 
    med R-paketet [rccKPL](https://bitbucket.org/cancercentrum/rcckpl)
  - NKBC Vården i siffror, 
    med R-paketet [incavis](https://bitbucket.org/cancercentrum/incavis)
- Användning lokalt på RCC Stockholm-Gotland tillsammans med R-paketet [nkbcgeneral](https://bitbucket.org/cancercentrum/nkbcgeneral)
  - Framtagande av NKBC Interaktiva Årsrapport med R-paketet [rccShiny](https://bitbucket.org/cancercentrum/rccshiny)

Jfr https://www.cancercentrum.se/samverkan/vara-uppdrag/statistik/kvalitetsregisterstatistik/

## Installation

```{r, eval = FALSE}
if (!requireNamespace("remotes")) {
  install.packages("remotes")
}

remotes::install_bitbucket("cancercentrum/nkbcind")
```

## Användning

```{r}
library(nkbcind)
```

Läs in ögonblicksbild av NKBC exporterad från INCA.

```{r}
load(
  file.path(Sys.getenv("BRCA_DATA_DIR"), "2022-09-02", "nkbc_nat_avid 2022-09-02 07-08-17.RData")
)
```

Generell förbearbetning av NKBC-data.

```{r}
df_nkbc_nat_avid_w_d_vars <- df |>
  dplyr::mutate(dplyr::across(tidyselect::where(is.factor), as.character)) |>
  dplyr::rename_with(
    # Döp om INCA:s variabelsuffix "_Värde" till "_Varde" eftersom
    # R-paket inte kan byggas om variabelnamn innehåller specialtecken
    stringr::str_replace, tidyselect::ends_with("_Värde"),
    pattern = "_Värde", replacement = "_Varde"
  ) |>
  nkbcgeneral::clean_nkbc_data() |>
  nkbcgeneral::mutate_nkbc_derived_vars()
```

Skapa generell dataram för redovisning av kvalitetsindikatorer och populationskarakteristik.

```{r}
df_nkbc_nat_avid_w_nkbcind_d_vars <- df_nkbc_nat_avid_w_d_vars |>
  nkbcind::mutate_nkbcind_derived_vars() |>
  dplyr::mutate(period = lubridate::year(a_diag_dat)) |>
  dplyr::filter(period %in% 2008:2021)
```

### Exempel: Screeningupptäckt bröstcancer

Titta på objektet `nkbc01` som definierar kvalitetsindikatorn "Screeningupptäckt bröstcancer" och används i de olika utdata-kanalerna från NKBC. 

```{r}
print(nkbc01)
```

Extrahera information om kvalitetsindikatorn såsom den presenteras i rccShiny-baserade interaktiva rapporter.

```{r}
cat(
  paste(
    nkbcind::get_rccShiny2_arg_outcomeTitle(nkbc01)["sv"],
    nkbcind::get_rccShiny2_arg_textBeforeSubtitle(nkbc01)["sv"],
    sep = "\n"
  )
)
```

Om indikatorn.

```{r}
cat(
  nkbcind::get_rccShiny2_arg_description(nkbc01, show_target_values = TRUE)[["sv"]][1]
)
```

Att tänka på vid tolkning.

```{r}
cat(
  nkbcind::get_rccShiny2_arg_description(nkbc01, show_target_values = TRUE)[["sv"]][2]
)
```

Teknisk beskrivning.

```{r}
cat(
  nkbcind::get_rccShiny2_arg_description(nkbc01, show_target_values = TRUE)[["sv"]][3]
)
```

Specifik databearbetning för kvalitetsindikatorn.

```{r}
df_nkbc01 <- df_nkbc_nat_avid_w_nkbcind_d_vars |>
  nkbcind::filter_pop(nkbc01)() |>
  nkbcind::mutate_outcome(nkbc01)()
```

Titta på bearbeatad data.

```{r}
df_nkbc01 |>
  dplyr::mutate(dplyr::across(dplyr::any_of("KON_VALUE"), as.factor)) |>
  dplyr::mutate(dplyr::across(tidyselect::ends_with("_Varde"), as.factor)) |>
  dplyr::select(KON_VALUE, a_pat_alder, a_diag_screening_Varde, outcome) |>
  summary()
```

```{r}
df_nkbc01 |>
  dplyr::select(period, outcome) |>
  table(useNA = "ifany")
```

Jfr https://statistik.incanet.se/brostcancer/ > Diagnostik > Screeningupptäckt bröstcancer
